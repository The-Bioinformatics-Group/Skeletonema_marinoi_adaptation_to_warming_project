# Redoing the mapping to v1.1.2 of the R05 reference genome

Includes a few resequenced strains which weren't included in the first mapping analysis
* The first 15 bp of these sequences were trimmed during QC
