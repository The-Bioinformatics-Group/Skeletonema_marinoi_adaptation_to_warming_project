# Mapping runs completed

Subsets.tar.gz in _101 - _104 being kept just in case
* Results of subset mapping are consistent with full set mapping

Note - the *.sge.e* file for some jobs contain an extra line regarding merging; no other errors appear to exist,
so this may be trivial (seems to primarily be for samples with overall alignment >50%)
