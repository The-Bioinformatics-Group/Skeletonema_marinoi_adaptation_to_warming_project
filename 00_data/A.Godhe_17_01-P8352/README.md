# NB

In cases where there are two sets of PE fastq.gz files in a directory, these have been split into two separate subdirectories



# Is further trimming required?

## RAW_TruSeqAdapterSequences
Sequences and index numbers of each adapter in each sample

## RAW_TruSeqAdapterSequencesFull
Sequences, index and stats of each adapter in each sample

## RAW_TruSeqAdapterSequencesOnly
Unique sequences of each adapter
