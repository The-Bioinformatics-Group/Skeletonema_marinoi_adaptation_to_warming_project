# Data handling
Data from UPPMAX, rsynced from /proj/data17/RAW_DATA to ./A.Godhe_17_01-P8352
* Checksums all OK

FastQC has been run on all .fastq.gz files
* See A.Godhe_17_01-P8352/runFastQC.sge

MultiQC has been run on all FastQC output
* See runMultiQC.sge, multiqc_data/ and multiqc_report.html (and interactive equivalents)
  * All in A.Godhe_17_01-P8352/

After viewing the FastQC and MultiQC reports, all reported adapters weres targeted for removal with Trim Galore
* See Trimmed/prepare_sequences_A2W_Template.sh

FastQC and MultiQC have been run on all trimmed .fq.gz files
* See runInteractiveMultiQC.sge, interactive_multiqc_data and interactive_multiqc_report.html
  * All in Trimmed/

All adapter sequences have been removed
* "[...] less than 1% of reads made up of overrepresented sequences."
* "No samples found with any adapter contamination > 0.1%."

Note - Four samples still report failure in the Sequence Quality Histogram of FastQC/MultiQC
* "This module will raise a failure if the lower quartile for any base is less than 5 or if the median for any base is less than 20."
  * P8352_132_S18_L002_R1_001.CA.FQF
  * P8352_132_S18_L003_R1_001.CA.FQF
  * P8352_136_S19_L002_R1_001.CA.FQF
  * P8352_136_S19_L003_R1_001.CA.FQF


# Sample details
(Additional details can be found at A.Godhe_17_01-P8352/00-Reports/A.Godhe_17_01_sample_info.txt)

## P8352_101 - P8352_112

LO-03
* Taken from fjord **unaffected by** power station, at 3cm depth (dated to ~2000 - post-power plant)

## P8352_113 - P8352_124

LO-07
* Taken from fjord **affected by** power station, at 7cm depth (dated to ~2000 - post-power plant)

## P8352_125 - P8352_136

LO-11
* Taken from fjord **unaffected by** power station, at 11cm depth (dated to 1960s - pre-power plant)

## P8352_137 - P8352_150

LO-21
* Taken from fjord **affected by** power station, at 21cm depth (dated to 1960s - pre-power plant)
