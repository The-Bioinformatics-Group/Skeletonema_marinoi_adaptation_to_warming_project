Here is a brief overview on the Skeletonema temperature adaptation project, including sampling sites,
experimental design, and the preliminary results we got from the temperature assays. I think that we got
sequences for most of the 48 strains we used in these temperature assays. It is possible that there are less
than 12 samples from some groups because we didn't get sufficient DNA for some strains and sequencing may have
failed for those strains.

We sampled sediment cores from two fjords in the Loviisa area in Finland. The LOW (Loviisa warm) core was taken
from a fjord where the Loviisa Nuclear Power Plant has increased water temperatures by 2-5°C due to coolin
 water discharges since the late 1970s. From this core we took two layers, 7cm that was dated around 2000 and
21cm that was dated to the 1960s, before the power plant started service. The LOC (Loviisa control) core was
taken from a fjord nearby where we expect similar conditions as in the Loviisa fjord but no cooling water
discharges. From the control core we worked with the 3cm layer (2000s) and the 11cm layer (1960s).  

From these sediment layers we revived resting stages of Skeletonema marinoi in the lab. We got 12 clonal 
isolates from each sediment layer, i.e. 12 isolates from the LOW 7cm layer, 12 isolates from the LOW 21cm layer,
12 isolates from the LOC 3cm layer and 12 isolates from the LOC 11cm layer, 48 isolates in total. These isolates
were taken into cultures maintained in the lab and then used for the temperature assay experiments we did in
spring and for sequencing.

The first results we got from the temperature assays look very promising, they indicate that there are
differences in temperature depended growth between strains from the LOW 7cm group compared to the other groups
that had not experience decades of cooling water discharges. We are currently work on the analysis of this data.

The strains are labeled x-y where x is the sediment layer/core and y is the strain name. So all 12 samples
labeled 21-y come from the LOW 21cm layer and are considered as the group of strains that lived in the Loviisa
fjord in the 1960s before the power plant was built. With the sequencing data we aim at identifying genomic
differences between these 4 strain groups and potentially even look at individual strains. The idea is that for
instance all 12 strains from the LOW 7cm group should have some common genetic properties that are relevant to
succeed in the warmer environment while these mutations/variants should be absent or at least low in frequency
in the other groups. 
