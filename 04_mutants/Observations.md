# Identifying potential LoF mutations in warm and cold strains

P8352_101-P8352_112 - cold
P8352_125-P8352_136 - cold

P8352_113-P8352_124 - warm
P8352_137-P8352_150 - warm

## Checking P8352_113-P8352_118

### Contig 000202F
* 29,014-29,016 - 3bp deletion to one side of a short repeat region ([CTA]CTGCTGCTGCTGCCGCTTCC...)
  * Homozygous deletion in 113, 114, 115, 117
  * Heterozygous in 118
  * 3' end of Sm_00029245-RA - BLASTp: predicted protein
* This deletion is also seen in cold samples

* 34,208 - 1bp deletion in middle of a gene
  * Homozygous deletion in 115, 117
  * Middle of Sm_00029248-RA - BLASTp: no good hits...
* Not in P8352_101-P8352_112 (cold)

### Contig 000138F
* 36,709 - 1bp deletion, but not in a coding region...
  * Heterozygous in 113, 115, 116, 117, 118
    * In particular, 115 has some consistent insertions and SNPs just upstream

* 38,586 - 1bp deletion, but not in a coding region...
  * Homozygous deletion in 116
  * Heterozygous in 113. 114, 117, 118
    * Consistent SNPs beside the deletion in each of these sample

* 40,381-40,383 - 3bp deletion, but not in coding region...
  * Heterozygous in 113, 114, 116, 118
    * In particular, deletion in 116 seems associated with an A>G substitution a few bases upstream

### Contig 000189F
* 1,913-1,916 - 4bp deletion, nested between a repeat? (CTC TAACAA[TAAG]TAACAA AAC)
  * Homozygous deletion in 115,117, 118
  * Heterozygous in 113, 116
    * In 113, 116, 117 and 118, there are ~consistent SNPs about 20bp upstream
  * Deletion falls within predicted intron of Sm_00028885-RA - BLASTp: 1,2-dihydroxy-3-keto-5-methylthiopentene dioxygenase
    * Involved in methionine biosynthesis

* 5,188-5,197 - 10bp deletion
  * Homozygous deletion in 113, 115 (some coverage in the gap...), 116, 117
  * Heterozygous in 114, 118
    * Deletion appears with a T>A SNP 2bp downstream of the deletion, and another ~30bp downstream
  * Deletion falls in an exon towards the 5' end of Sm_00028887-RA - BLASTp: phosphoenol pyruvate synthase
    * Assuming that the cDNA sequence from the genome browser is correct, this would severely disrupt the coding sequence
    * However, potentially other copies (e.g. Sm_00020569-RA is similar, and BLASTp also hits phosphoenol pyruvate synthases)

* 5,779-5,787 - 9bp deletion
  * Homozygous deletion in 114, 116, 117
  * Heterozygous in 113, 115, 118 (may be a deletion; coverage ~1/3 of surrounding area)
    * Deletion appears with a C>A SNP 3bp upstream of the deletion, and G>T sub ~20bp upstream
  * Deletion falls in an exon in the middle of Sm_00028887-RA (see above)
    * In-frame deletion, so wouldn't disrupt coding sequence

* 7,364 - 1bp deletion
  * Homozygous deletion in all 6 samples
    * A few reads cover the gap, but almost all are at the end of reads so could be sequencing errors?
  * Deletion falls in an exon towards the 3' end of Sm_00028887-RA (see above)
    * This disrupts the final ~1/3 of the protein
* This deletion also appears in some cold samples

* 9,231-9,232 - 2bp deletion, but not in a coding region...
  * Homozygous deletion in 113, 114, 116, 117
  * Heterozygous in 115, 118
    * Appears to fall outside of any coding regions...

* 13,408-13,409 - 2bp deletion, just upstream of a coding region...
  * Homozygous deletion in all 6 samples
    * Deletion falls just upstream of Sm_00028888-RA - BLASTp: glutamine synthetase? (only ~50% similarity...)
* This deletion also appears in cold samples










(Note: Contig 000028F contains FAT1 (pos ~684,369-689,279, but will differ slightly as this is the v1.0 location))
