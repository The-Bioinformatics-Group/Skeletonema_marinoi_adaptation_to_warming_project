# Identifying Warm frameshift mutations

A list of Warm frameshift heterozygous/HomoDel mutations has been identified. This list has been passed to a
Python script (exon_check.py) to identify which of these fall within exons.

## Caveats

Note 1: If the mutation is similar to a Cold feature (e.g. 1bp deletion in Warm vs. 2bp deletion in Cold at the
        same position), this will still be counted as a unique Warm feature.
Note 2: If it's a >1bp mutation which starts outside an exon but then continues into one, this won't be flagged.
Note 3: The annotation file referenced is for the pre-corrected (v1.0) S. marinoi sequence, so some positions
        may be incorrect.
Note 4: Where there are overlapping exon predictions, only one of these is counted.
Note 5: This approach will consider all such mutations appearing in Warm samples, even if they appear in only
        one; this should probably be refined at a later date.
        * Running another instance looking only at mutations in 2+ samples
Note 6: The default coverage cutoff of 20x was used, therefore instances with a lower coverage may have been missed
        * e.g. 000018F:871,183

## Results

Examples found in 1+ samples: 46,011
Examples found in 2+ samples:  5,696
