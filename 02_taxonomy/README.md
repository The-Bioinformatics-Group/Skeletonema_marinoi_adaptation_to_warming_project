# Introduction
We are extracting the 5.8S rRNA, ITS2, 28S rRNA regions from all strains to see how closely rellated they are.
This is a multi copy region so for this analysis we are using the region 1769-4826 on contig 000330F.

# Analysis
Commit dc65b9a3337947b2e0e8b8ee29dc6695dc176550 of `bamboozle.py` was used for the analysis.

```bash
for_each_dir_do.sh "/home/mtop/git/Bamboozle/bamboozle.py -r /home/mtop/git/Bamboozle/example/Skeletonema_marinoi_Ref_v1.1.1.fst -b *.bam --mapping_quality 30 -c 000330F --range 1769-4826" > ITS2.fst
```

The sequences have been reversed.
