# Double-checking identity of Skeletonema within the samples

Most samples' metagenome assemblies contain chloroplastic rbcL (with 11 exceptions)
* When put into a tree with trimmed sequences and other Skeletonema samples, these samples separated
  in the expected way, i.e. the high-coverage samples were in a clade with S. marinoi, and the low-coverage
  samples were in a clade with S. subsalsum
* One outlier - _105 on its own branch near the root
  * Double-checked with outgrouping to T. pseudonana; same result

Using cycloartenol synthase partial sequences, and outgrouping against T. pseudonana, the samples cluster
more or less as expected (a few suspected S. marinoi samples appear a little more distant, but still form a
monophyletic group)
* This was done using Clustal Omega, so no support values, etc. Rerun using a more accurate program?
  * Fasttree bootstrap values reflect this fairly well
  * No outliers
(Note: _104 and _105 sequences found when using T. pseudonana as query)

coxI tree of all available Skeletonema sequences, and outgrouping against T. pseudonana, also shows more or
less the expected pattern (with a few of the A2W samples outlying)
* _104 and _105 on their own branch near the root

16S rRNA tree shows more or less the expected pattern (with a few A2W outliers)
* _104 and _105 on their own branch near the root

psbC tree shows more or less the expected pattern (with one A2W outlier - P8352_105)
* _105 on its own branch near the root

Fewer usable SIT sequences in bins, but those which appeared retained the expected pattern
* No outliers

Note: Where _104 and _105 are not listed as outliers, they had no sequence in the analysis

_104 and _105 mapped the lowest to the S. marinoi R05 reference genome (<2%), and in midpoint-rooted trees,
they appear on a branch with T. pseudonana...

| Sequence  |                                         BLAST result                                         |
|-----------|----------------------------------------------------------------------------------------------|
| _104 coxI | 100% ID to coxI in T. pseudonana                                                             |
| _104 16S  | 100% ID to 16S in T. pseudonana; 99% ID to other diatoms, inc. S. costatum + pseudocostatum  |
|-----------|----------------------------------------------------------------------------------------------|
| _105 coxI | 100% ID to coxI in T. pseudonana                                                             |
| _105 psbC | 100% ID to psbC in T. pseudonana; >96% ID to other diatoms, inc S. subsalsum (95% ID)        |
| _105 rbcL | 99% ID to rbcL in T. pseudonana; >96% ID to other diatoms, inc. several Skeletonema (95% ID) |
| _105 16S  | 100% ID to 16S in T. pseudonana; 99% ID to other diatoms, inc. S. costatum + pseudocostatum  |

* coxI and 16S are identical in _104 and _105

BLAST of first two contigs in `P8352_104/bin.1.fa`
* P8352_104_k141_62231: 90+% matches to T. pseudonana (inc. retrotransposon)
* P8352_104_k141_16646: 99% match to T. pseudonana

BLAST of first two contigs in `P8352_105/bin.1.fa`
* P8352_105_k141_51646: Only very short matches
* P8352_105_k141_48652: 99% match to T. pseudonana (19% query cover...)

(Compare with a suspected S. marinoi sequence to ensure this result isn't misinterpreted)
* First two contigs in `P8352_113/bin.1.fa`
  * P8352_113_k141_90287: 77% match to T. pseudonana (19% query cover...)
  * P8352_113_k141_19008: 74% match to T. pseudonana (28% query cover...)

## Failed checks
Searching the metagenome assemblies for sequences associated with rRNA genes (18S, LSU) turned up either
short and/or fragmented results

## Any other tests?
