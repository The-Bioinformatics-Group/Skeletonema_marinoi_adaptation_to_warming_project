|  Sample   | Mapping |     Core      |   rbcL    |   cyclo   |   coxI    |    16S    |   psbC    |    SIT    |    Summary     |
|-----------|---------|---------------|-----------|-----------|-----------|-----------|-----------|-----------|----------------|
| P8352_101 |  69.67% | Control, 2000 |  marinoi  |  marinoi  |  marinoi  |  marinoi  |  marinoi  |  marinoi  | S. marinoi     |
| P8352_102 |   3.31% | Control, 2000 | subsalsum | subsalsum | subsalsum | subsalsum | subsalsum | subsalsum | S. subsalsum   |
| P8352_103 |  42.68% | Control, 2000 |  marinoi  |  marinoi* |  marinoi  |  marinoi  |  marinoi  |    N/A    | S. marinoi     |
| P8352_104 |   1.15% | Control, 2000 |    N/A    |     ?     | T. pseud? | T. pseud? |    N/A    |    N/A    | Thalassiosira? |
| P8352_105 |   1.70% | Control, 2000 | T. pseud? |     ?     | T. pseud? | T. pseud? | T. pseud? |    N/A    | Thalassiosira? |
| P8352_106 |   6.74% | Control, 2000 | subsalsum | subsalsum |    N/A    | subsalsum | subsalsum | subsalsum | S. subsalsum   |
| P8352_107 |   4.27% | Control, 2000 | subsalsum | subsalsum | subsalsum | subsalsum | subsalsum | subsalsum | S. subsalsum   |
| P8352_108 |  61.74% | Control, 2000 |    N/A    |  marinoi  |    N/A    |    N/A    |    N/A    |    N/A    | marinoi?       |
| P8352_109 |  46.34% | Control, 2000 |  marinoi  |  marinoi  |  marinoi  |  marinoi  |  marinoi  |    N/A    | S. marinoi     |
| P8352_110 |   8.52% | Control, 2000 |    N/A    | subsalsum |    N/A    |    N/A    |    N/A    |    N/A    | subsalsum?     |
| P8352_111 |  79.80% | Control, 2000 |  marinoi  |  marinoi  |  marinoi* |  marinoi  |  marinoi  |    N/A    | S. marinoi     |
| P8352_112 |  51.19% | Control, 2000 |  marinoi  |  marinoi  |  marinoi  |  marinoi  |  marinoi  |    N/A    | S. marinoi     |
|-----------|---------|---------------|-----------|-----------|-----------|-----------|-----------|-----------|----------------|
| P8352_113 |  73.51% | Loviisa, 2000 |  marinoi  |  marinoi  |  marinoi  |  marinoi  |  marinoi  |    N/A    | S. marinoi     |
| P8352_114 |  77.95% | Loviisa, 2000 |  marinoi  |  marinoi  |  marinoi  |  marinoi  |  marinoi  |    N/A    | S. marinoi     |
| P8352_115 |  71.50% | Loviisa, 2000 |  marinoi  |  marinoi  |    N/A    |  marinoi  |  marinoi  |    N/A    | S. marinoi     |
| P8352_116 |  70.93% | Loviisa, 2000 |    N/A    |  marinoi  |    N/A    |    N/A    |    N/A    |    N/A    | marinoi?       |
| P8352_117 |  60.15% | Loviisa, 2000 |    N/A    |  marinoi  |  marinoi* |    N/A    |    N/A    |    N/A    | marinoi?       |
| P8352_118 |  82.44% | Loviisa, 2000 |  marinoi  |  marinoi  |  marinoi  |  marinoi  |  marinoi  |    N/A    | S. marinoi     |
| P8352_119 |  30.76% | Loviisa, 2000 |  marinoi  |  marinoi  |    N/A    |  marinoi  |  marinoi  |    N/A    | S. marinoi     |
| P8352_120 |  65.24% | Loviisa, 2000 |  marinoi  |  marinoi  |  marinoi  |  marinoi  |  marinoi  |    N/A    | S. marinoi     |
| P8352_121 |  47.86% | Loviisa, 2000 |  marinoi  |  marinoi  |    N/A    |  marinoi  |  marinoi  |    N/A    | S. marinoi     |
| P8352_122 |  59.13% | Loviisa, 2000 |  marinoi  |  marinoi  |  marinoi  |  marinoi  |  marinoi  |    N/A    | S. marinoi     |
| P8352_123 |  53.17% | Loviisa, 2000 |  marinoi  |  marinoi  |  marinoi  |  marinoi  |  marinoi  |    N/A    | S. marinoi     |
| P8352_124 |  74.22% | Loviisa, 2000 |  marinoi  |  marinoi  |  marinoi  |  marinoi  |  marinoi  |    N/A    | S. marinoi     |
|-----------|---------|---------------|-----------|-----------|-----------|-----------|-----------|-----------|----------------|
| P8352_125 |   4.13% | Control, 1960 |    N/A    | subsalsum |    N/A    |    N/A    |    N/A    | subsalsum | subsalsum?     |
| P8352_126 |   6.41% | Control, 1960 | subsalsum | subsalsum | subsalsum | subsalsum | subsalsum |    N/A    | S. subsalsum   |
| P8352_127 |   3.54% | Control, 1960 | subsalsum | subsalsum | subsalsum | subsalsum | subsalsum |    N/A    | S. subsalsum   |
| P8352_128 |   8.08% | Control, 1960 | subsalsum | subsalsum | subsalsum | subsalsum | subsalsum | subsalsum | S. subsalsum   |
| P8352_129 |   2.80% | Control, 1960 | subsalsum | subsalsum | subsalsum | subsalsum | subsalsum | subsalsum | S. subsalsum   |
| P8352_130 |   2.64% | Control, 1960 | subsalsum | subsalsum |    N/A    | subsalsum | subsalsum | subsalsum | S. subsalsum   |
| P8352_131 |   4.49% | Control, 1960 |    N/A    | subsalsum | subsalsum |    N/A    |    N/A    |    N/A    | subsalsum?     |
| P8352_132 |   5.16% | Control, 1960 | subsalsum | subsalsum | subsalsum | subsalsum | subsalsum |    N/A    | S. subsalsum   |
| P8352_133 |   6.98% | Control, 1960 | subsalsum | subsalsum | subsalsum | subsalsum | subsalsum |    N/A    | S. subsalsum   |
| P8352_134 |   4.94% | Control, 1960 |    N/A    | subsalsum |    N/A    |    N/A    |    N/A    | subsalsum | subsalsum?     |
| P8352_135 |   5.05% | Control, 1960 | subsalsum | subsalsum | subsalsum | subsalsum | subsalsum |    N/A    | S. subsalsum   |
| P8352_136 |  74.05% | Control, 1960 |  marinoi  |  marinoi  |  marinoi  |  marinoi  |  marinoi  |    N/A    | S. marinoi     |
|-----------|---------|---------------|-----------|-----------|-----------|-----------|-----------|-----------|----------------|
| P8352_137 |   3.71% | Loviisa, 1960 | subsalsum | subsalsum |    N/A    | subsalsum | subsalsum | subsalsum | S. subsalsum   |
| P8352_138 |   6.66% | Loviisa, 1960 | subsalsum | subsalsum | subsalsum | subsalsum | subsalsum | subsalsum | S. subsalsum   |
| P8352_139 |   6.79% | Loviisa, 1960 | subsalsum | subsalsum | subsalsum | subsalsum | subsalsum |    N/A    | S. subsalsum   |
| P8352_140 |   7.94% | Loviisa, 1960 | subsalsum | subsalsum | subsalsum | subsalsum | subsalsum | subsalsum | S. subsalsum   |
| P8352_141 |   7.76% | Loviisa, 1960 |    N/A    | subsalsum |    N/A    |    N/A    |    N/A    | subsalsum | subsalsum?     |
| P8352_142 |   2.41% | Loviisa, 1960 | subsalsum | subsalsum | subsalsum | subsalsum | subsalsum | subsalsum | S. subsalsum   |
| P8352_143 |   3.91% | Loviisa, 1960 | subsalsum | subsalsum |    N/A    | subsalsum | subsalsum | subsalsum | S. subsalsum   |
| P8352_144 |   4.69% | Loviisa, 1960 | subsalsum | subsalsum |    N/A    | subsalsum | subsalsum |    N/A    | S. subsalsum   |
| P8352_145 |  77.21% | Loviisa, 1960 |  marinoi  |  marinoi  |  marinoi  |  marinoi  |  marinoi  |    N/A    | S. marinoi     |
| P8352_146 |  81.02% | Loviisa, 1960 |  marinoi  |  marinoi* |    N/A    |  marinoi  |  marinoi  |    N/A    | S. marinoi     |
| P8352_147 |  74.78% | Loviisa, 1960 |    N/A    |  marinoi* |    N/A    |    N/A    |    N/A    |    N/A    | marinoi?       |
| P8352_148 |   3.72% | Loviisa, 1960 | subsalsum | subsalsum | subsalsum | subsalsum | subsalsum | subsalsum | S. subsalsum   |
| P8352_149 |   8.93% | Loviisa, 1960 |    N/A    | subsalsum | subsalsum |    N/A    |    N/A    |    N/A    | subsalsum?     |
| P8352_150 |  73.86% | Loviisa, 1960 |  marinoi  |  marinoi  |  marinoi  |  marinoi  |  marinoi  |    N/A    | S. marinoi     |
