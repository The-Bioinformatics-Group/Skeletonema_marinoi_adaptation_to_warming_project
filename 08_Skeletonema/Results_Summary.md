|   Sample   |     ID     | Mapping to R05 ref. |     Core      |    Summary     |
|------------|------------|---------------------|---------------|----------------|
| P8352_101  | LO-03-14   |       69.67%        | Control, 2000 | S. marinoi     |
| P8352_102  | LO-03-20   |        3.31%        | Control, 2000 | S. subsalsum   |
| P8352_103  | LO-03-51   |       42.68%        | Control, 2000 | S. marinoi     |
| P8352_104  | LO-03-52   |        1.15%        | Control, 2000 | Thalassiosira? |
| P8352_105  | LO-03-57   |        1.70%        | Control, 2000 | Thalassiosira? |
| P8352_106  | LO-03-75   |        6.74%        | Control, 2000 | S. subsalsum   |
| P8352_107  | LO-03-77   |        4.27%        | Control, 2000 | S. subsalsum   |
| P8352_108  | LO-03-01   |       61.74%        | Control, 2000 | marinoi?       |
| P8352_109  | LO-03-04   |       46.34%        | Control, 2000 | S. marinoi     |
| P8352_110  | LO-03-06   |        8.52%        | Control, 2000 | subsalsum?     |
| P8352_111  | LO-03-07   |       79.80%        | Control, 2000 | S. marinoi     |
| P8352_112  | LO-03-63   |       51.19%        | Control, 2000 | S. marinoi     |
| P14203_159 | LO-03-1902 |       82.15%        | Control, 2000 | S. marinoi     |
| P14203_161 | LO-03-1901 |       65.72%        | Control, 2000 | S. marinoi     |
| P16216_101 | LO-03-80   |        ...          | Control, 2000 | S. marinoi     |
| P16216_102 | LO-03-71   |        ...          | Control, 2000 | S. marinoi     |
| P16216_103 | LO-03-62   |        ...          | Control, 2000 | S. marinoi     |
| P16216_104 | LO-03-55   |        ...          | Control, 2000 | S. marinoi     |
|------------|------------|---------------------|---------------|----------------|
| P8352_113  | LO-07-03   |       73.51%        | Loviisa, 2000 | S. marinoi     |
| P8352_114  | LO-07-05   |       77.95%        | Loviisa, 2000 | S. marinoi     |
| P8352_115  | LO-07-09   |       71.50%        | Loviisa, 2000 | S. marinoi     |
| P8352_116  | LO-07-21   |       70.93%        | Loviisa, 2000 | marinoi?       |
| P8352_117  | LO-07-22   |       60.15%        | Loviisa, 2000 | marinoi?       |
| P8352_118  | LO-07-24   |       82.44%        | Loviisa, 2000 | S. marinoi     |
| P8352_119  | LO-07-25   |       30.76%        | Loviisa, 2000 | S. marinoi     |
| P8352_120  | LO-07-27   |       65.24%        | Loviisa, 2000 | S. marinoi     |
| P8352_121  | LO-07-31   |       47.86%        | Loviisa, 2000 | S. marinoi     |
| P8352_122  | LO-07-34   |       59.13%        | Loviisa, 2000 | S. marinoi     |
| P8352_123  | LO-07-52   |       53.17%        | Loviisa, 2000 | S. marinoi     |
| P8352_124  | LO-07-54   |       74.22%        | Loviisa, 2000 | S. marinoi     |
| P16216_105 | LO-07-50   |        ...          | Loviisa, 2000 | S. marinoi     |
|------------|------------|---------------------|---------------|----------------|
| P8352_125  | LO-11-01   |        4.13%        | Control, 1960 | subsalsum?     |
| P8352_126  | LO-11-03   |        6.41%        | Control, 1960 | S. subsalsum   |
| P8352_127  | LO-11-54   |        3.54%        | Control, 1960 | S. subsalsum   |
| P8352_128  | LO-11-55   |        8.08%        | Control, 1960 | S. subsalsum   |
| P8352_129  | LO-11-59   |        2.80%        | Control, 1960 | S. subsalsum   |
| P8352_130  | LO-11-62   |        2.64%        | Control, 1960 | S. subsalsum   |
| P8352_131  | LO-11-63   |        4.49%        | Control, 1960 | subsalsum?     |
| P8352_132  | LO-11-75   |        5.16%        | Control, 1960 | S. subsalsum   |
| P8352_133  | LO-11-76   |        6.98%        | Control, 1960 | S. subsalsum   |
| P8352_134  | LO-11-58   |        4.94%        | Control, 1960 | subsalsum?     |
| P8352_135  | LO-11-61   |        5.05%        | Control, 1960 | S. subsalsum   |
| P8352_136  | LO-11-67   |       74.05%        | Control, 1960 | S. marinoi     |
| P14203_157 | LO-11-88   |   6.19% (v1.1.2)    | Control, 1960 | S. subsalsum   |
| P16216_107 | LO-11-57   |        ...          | Control, 1960 | S. subsalsum   |
| P16216_108 | LO-11-86   |        ...          | Control, 1960 | S. subsalsum   |
|------------|------------|---------------------|---------------|----------------|
| P8352_137  | LO-21-51   |        3.71%        | Loviisa, 1960 | S. subsalsum   |
| P8352_138  | LO-21-52   |        6.66%        | Loviisa, 1960 | S. subsalsum   |
| P8352_139  | LO-21-55   |        6.79%        | Loviisa, 1960 | S. subsalsum   |
| P8352_140  | LO-21-56   |        7.94%        | Loviisa, 1960 | S. subsalsum   |
| P8352_141  | LO-21-58_a |        7.76%        | Loviisa, 1960 | subsalsum?     |
| P8352_142  | LO-21-58_b |        2.41%        | Loviisa, 1960 | S. subsalsum   |
| P8352_143  | LO-21-61   |        3.91%        | Loviisa, 1960 | S. subsalsum   |
| P8352_144  | LO-21-68   |        4.69%        | Loviisa, 1960 | S. subsalsum   |
| P8352_145  | LO-21-29   |       77.21%        | Loviisa, 1960 | S. marinoi     |
| P8352_146  | LO-21-30   |       81.02%        | Loviisa, 1960 | S. marinoi     |
| P8352_147  | LO-21-53   |       74.78%        | Loviisa, 1960 | marinoi?       |
| P8352_148  | LO-21-54_a |        3.72%        | Loviisa, 1960 | S. subsalsum   |
| P8352_149  | LO-21-54_b |        8.93%        | Loviisa, 1960 | subsalsum?     |
| P8352_150  | LO.21-60   |       73.86%        | Loviisa, 1960 | S. marinoi     |
| P14203_158 | LO-21-04   |   71.92% (v1.1.2)   | Loviisa, 1960 | S. marinoi     |
| P14203_160 | LO-21-57   |   73.55% (v1.1.2)   | Loviisa, 1960 | S. marinoi     |
| P16216_106 | LO-21-67   |        ...          | Loviisa, 1960 | S. subsalsum   |
| P16216_109 | LO-21-63   |        ...          | Loviisa, 1960 | S. subsalsum   |
