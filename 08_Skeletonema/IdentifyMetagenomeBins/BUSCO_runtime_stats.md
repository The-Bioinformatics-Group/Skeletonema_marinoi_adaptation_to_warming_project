| Sample | Approx time taken | Cores |     Node     | Consecutive jobs |
|--------|-------------------|-------|--------------|------------------|
|  _102  |      ~1 hour      |  20   | Annotation-1 |        1         |
|  _104  | ~2 hours 10 mins  |  40   |   high_mem   |        3         |
|  _105  |      ~35 mins     |  40   | Annotation-2 |        1         |
|  _106  | ~1 hour 45 mins   |  20   |    node0     |        1         |
|  _107  |      ~40 mins     |  16   |  sandbox 10  |        1         |
|  _110  |         ?         |   ?   |      ?       |        1         | *
|  _125  |      ~1 hour      |   8   |  sandbox 12  |        1         |
|  _126  | ~2 hours 45 mins  |  16   |  sandbox 13  |        3         |
|  _127  |      ~35 mins     |  20   |   high_mem   |        1         |
|  _128  |      ~2 hours     |  20   | Annotation-1 |        3         |
|  _129  |  ~1 hour 30 mins  |  40   | Annotation-2 |        3         |
| _130_a |      ~1 hour      |  40   | Annotation-1 |        1         |
| _130_b |      ~50 mins     |  16   |  sandbox 10  |        2         |
|  _131  |      ~55 mins     |  20   |   high_mem   |        1         |
|  _132  |  ~1 hour 15 mins  |  16   |  sandbox 10  |        1         |
|  _133  |      ~1 hour      |  40   |    node0     |        1         |
|  _134  |      ~30 mins     |  20   | Annotation-1 |        1         |
|  _135  | ~2 hours 25 mins  |  40   |    node0     |        3         |
|  _137  |      ~1 hour      |  18   | Annotation-1 |        1         |
|  _138  |      ~35 mins     |  18   | Annotation-1 |        1         |
|  _139  |      ~1 hour      |  16   |  sandbox 12  |        1         |
|  _140  |      ~55 mins     |  20   |   high_mem   |        1         |
|  _141  |  ~1 hour 40 mins  |  40   | Annotation-2 |        3         |
|  _142  |  ~1 hour 40 mins  |  20   |   high_mem   |        1         |
|  _143  |      ~45 mins     |  20   |   high_mem   |        1         |
|  _144  |      ~1 hour      |  18   | Annotation-1 |        1         |
|  _148  |      ~50 mins     |  20   |    node0     |        1         |
|  _149  |      ~30 mins     |  35   | Annotation-1 |        1         |
