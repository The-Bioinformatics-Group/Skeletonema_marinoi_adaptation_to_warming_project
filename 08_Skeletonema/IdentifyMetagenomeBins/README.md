# Determining the amount of diatom data present in the samples which map poorly to R05

## Results

| Sample |      Most tBLASTx hits to R05      | Longest ctg | Bowtie2 | BUSCO |
|--------|------------------------------------|-------------|---------|-------|
|  _102  |  Bin 1   (4,086 contigs; 35.9 Mbp) |  58,764 bp  | 26.01%  | 79.6% |
| _104_1 |  Bin 1   (2,479 contigs; 19.1 Mbp) |  56,585 bp  | 22.98%  | 55.1% |
| _104_2 |  Bin 2   (  525 contigs;  9.2 Mbp) | 133,460 bp  | 11.17%  | 29.0% |
| _104_X | Bin 1+2  (3,004 contigs; 28.2 Mbp) | ----------- | ------- | 79.3% |
|  _105  |  Bin 1   (2,737 contigs; 27.3 Mbp) |  78,071 bp  | 69.50%  | 72.0% |
|  _106  |  Bin 1   (4,070 contigs; 35.7 Mbp) |  58,864 bp  | 35.98%  | 81.2% |
|  _107  |  Bin 1   (3,939 contigs; 35.0 Mbp) |  66,791 bp  | 31.74%  | 80.2% |
|  _110  |  Bin 1   (4,178 contigs; 36.1 Mbp) |  66,231 bp  | 46.06%  | 79.6% |
|  _125  |  Bin 1   (3,966 contigs; 34.9 Mbp) |  66,151 bp  | 27.67%  | 79.8% |
| _126_1 |  Bin 1   (4,060 contigs; 35.2 Mbp) |  58,897 bp  | 49.15%  | 79.2% |
|_126_15?|(Bin 15?) (   47 contigs;  0.3 Mbp) |  22,001 bp  |  0.47%  |  0.7% |
| _126_X | Bin 1+15 (4,107 contigs; 35.6 Mbp) | ----------- | ------- | 79.2% |
|  _127  |  Bin 1   (4,171 contigs; 36.2 Mbp) |  66,032 bp  | 27.59%  | 81.5% |
| _128_1 |  Bin 1   (3,117 contigs; 29.2 Mbp) |  54,713 bp  | 46.64%  | 69.3% |
| _128_3?|(Bin 3?)  (  819 contigs;  5.3 Mbp) |  27,433 bp  |  9.14%  | 14.5% |
| _128_X | Bin 1+3  (3,936 contigs; 34.5 Mbp) | ----------- | ------- | 79.8% |
| _129_1 |  Bin 1   (1,930 contigs; 20.6 Mbp) |  66,428 bp  | 15.36%  | 56.4% |
| _129_2 |  Bin 2   (2,050 contigs; 14.9 Mbp) |  46,240 bp  | 10.32%  | 31.7% |
| _129_X | Bin 1+2  (3,980 contigs; 35.5 Mbp) | ----------- | ------- | 79.5% |
| _130_1 |  Bin 1   (3,215 contigs; 30.5 Mbp) |  60,858 bp  | 18.58%  | 72.3% |
| _130_9?|(Bin 9?)  (  594 contigs;  3.6 Mbp) |  23,616 bp  |  2.50%  |  9.6% |
| _130_X | Bin 1+9  (3,809 contigs; 34.1 Mbp) | ----------- | ------  | 77.9% |
|  _131  |  Bin 1   (3,996 contigs; 35.3 Mbp) |  56,247 bp  | 38.61%  | 79.2% |
|  _132  |  Bin 1   (4,139 contigs; 36.2 Mbp) |  54,722 bp  | 41.64%  | 78.9% |
|  _133  |  Bin 1   (3,742 contigs; 33.3 Mbp) |  67,568 bp  | 47.27%  | 76.5% |
|  _134  |  Bin 1   (4,106 contigs; 35.8 Mbp) |  66,533 bp  | 38.95%  | 80.2% |
| _135_1 |  Bin 1   (2,425 contigs; 24.8 Mbp) |  66,333 bp  | 31.77%  | 63.3% |
| _135_2 |  Bin 2   (1,646 contigs; 10.9 Mbp) |  38,312 bp  | 13.09%  | 24.1% |
| _135_X | Bin 1+2  (4,071 contigs; 35.7 Mbp) | ----------- | ------- | 80.5% |
|  _137  |  Bin 1   (4,107 contigs; 36.2 Mbp) |  63,813 bp  | 28.66%  | 81.5% |
|  _138  |  Bin 1   (4,093 contigs; 35.6 Mbp) |  66,161 bp  | 50.07%  | 79.8% |
|  _139  |  Bin 1   (3,976 contigs; 35.3 Mbp) |  60,707 bp  | 50.46%  | 80.5% |
|  _140  |  Bin 1   (4,051 contigs; 35.7 Mbp) |  58,752 bp  | 56.48%  | 79.2% |
| _141_1 |  Bin 1   (2,512 contigs; 25.0 Mbp) |  58,762 bp  | 39.97%  | 64.1% |
| _141_2 |  Bin 2   (1,600 contigs; 10.8 Mbp) |  46,098 bp  | 17.09%  | 24.4% |
| _141_X | Bin 1+2  (4,112 contigs; 35.9 Mbp) | ----------- | ------- | 79.5% |
|  _142  |  Bin 1   (4,159 contigs; 36.2 Mbp) |  58,869 bp  | 19.68%  | 79.5% |
|  _143  |  Bin 1   (3,916 contigs; 34.5 Mbp) |  56,211 bp  | 32.55%  | 78.9% |
|  _144  |  Bin 1   (3,492 contigs; 31.9 Mbp) |  57,561 bp  | 25.67%  | 71.0% |
|  _148  |  Bin 1   (4,098 contigs; 36.0 Mbp) |  66,129 bp  | 32.51%  | 80.5% |
|  _149  |  Bin 1   (4,122 contigs; 35.9 Mbp) |  66,383 bp  | 47.48%  | 81.8% |


## Pipeline

1. For file in 'bin.*.fa'; sed [append bin # to start of FASTA header], and save to alt-headers.bin.*.fa
* `for i in {1..X}; do sed "s/^>/>Bin${i}_/g" bin.${i}.fa > alt-headers.bin.${i}.fa; done`
* `sed 's/^>/>Unbinned_/g' bin.unbinned.fa > alt-headers.bin.unbinned.fa`

2. Concatenate these files into SAMPLE_alt-header_AllBins.fa
* `cat alt-headers.bin.*.fa > SAMPLE_alt-header_AllBins.fa`

3. Make BLAST DB of this file
* `makeblastdb -in SAMPLE_alt-header_AllBins.fa -dbtype nucl`

4. Run tBLASTx of R05 v1.1.1 against this database
5. Check the number of results for each bin (cut, sort, uniq), and stop the analysis when a clear result emerges
* `cut -f2 R05_vs_SAMPLE_AllBins.tBLASTx.txt | cut -f1 -d'_' | sort | uniq -c | sort -h`


6. Use Bowtie2 to map the sample's reads back to the suspected diatom bin
7. If there's time, check the mapping to the other bins as well

8. Run BUSCO on the diatom bin(s)
