# Workflow

* BLASTn concatenated primer sequences against RO5 v1.1.1 genome
* Determine which are present, and note the locations
* Extract the consensus sequence for each sample
  * Bamboozle?

# Summary

* Smar1 - One good hit on 000000F (and an associated contig)
  * However, part of the primer doesn't match exactly
* Region to use - 000000F : 646541-646732
* (BLAST hit - 646541-646729 - but missing three bases from reverse primer)

* Smar1 associated contig
* Region to use - 000000F-018-01 : 12337-12527
* (BLAST hit - 12337-12528 - one extra base added?)

* Smar2 - Many hits on 000009F (and an associated contig) and 000159F
  * Many of these are perfect hits (some are slightly mismatched), and occur fairly regularly; repeat region?
  * Pick a representative region
* Region to use - 000009F : 1422009-1422390

* Smar3 - One good hit on 000047F
  * One base mismatch in forward primer
* Region to use - 000047F : 66626-66803

* Smar4 - One good hit on 000001F (and an associated contig)
  * One base mismatch in forward primer on both occasions
* Region to use - 000001F : 1750659-1750778

* Smar4 associated contig
* Region to use - 000001F-013-01 : 58817-58936

* Smar5 - One good hit on 000045F
  * One base mismatch in reverse primer
* Region to use - 000045F : 291622-292026

* Smar6 - Many hits on many contigs
  * These hits are also fairly regular; repeat region(s)?
  * Pick a representative region
* Region to use - 000009F : 1500951-1501288

* Smar7 - No complete hits to reverse primer

* Smar8 - One perfect hit on 000140F
* Region to use - 000140F : 42745-42932

-----

# Locations

| Primers | Contig  |    Location     | Assoc contig?  |  Location   | Primer sizes |
|---------|---------|-----------------|----------------|-------------|--------------|
|  Smar1  | 000000F | 646541-'646729' | 000000F-018-01 | 12337-12528 |   23 + 22    |
|  Smar2  |                          MANY                            |   18 + 23    |
|  Smar3  | 000047F | 66626-66803     |      N/A       |     N/A     |   23 + 21    |
|  Smar4  | 000001F | 1750659-1750778 | 000001F-013-01 | 58817-58936 |   23 + 19    |
|  Smar5  | 000045F | 291622-292026   |      N/A       |     N/A     |   19 + 23    |
|  Smar6  |                          MANY                            |   23 + 18    |
|  Smar7  |                          ?                               |   27 + 20    |
|  Smar8  | 000140F | 42745-42932     |      N/A       |     N/A     |   20 + 20    |



Smar1
* 000000F-018-01
  * 12337-12528		OK (but last 3 bases of forward primer don't match)
* 000000F
  * 646541-646729	OK (last 3 bases of forward primer + first 3 bases of reverse primer don't match)
    * Use 646541-646732; 2/3 of the 'missing bases' are fine

Smar2
* 000009F
  * 1422009-1422390	OK
  * 1423875-1425605?	Possible pair, but more separated than expected
  * 1427066-1427443	OK
  * 1428908-1429287	OK
  * 1430747-1431126	One base mismatch in reverse primer
  * 1432587-1432966	One base mismatch in reverse primer
  * 1434426-1434807	One base mismatch in reverse primer
  * 1436268-1436649	OK
  * 1438150-1438535	One base mismatch in reverse primer
* 000159F
  * 57944-58321		OK
  * 61032-61411		One base mismatch in reverse primer
  * 62872-63259		One base mismatch in reverse primer
  * 64720-65099		One base mismatch in reverse primer
  * 66559-66942		One base mismatch in reverse primer
  * 68403-68782		One base mismatch in reverse primer
  * 70238-70613		One base mismatch in reverse primer
  * 72064-72436		OK
  * 73873-74243		OK (first 3 bases of reverse primer don't match)
  * 75656-77371?	Last 7 bases of reverse primer don't match; also more separated than expected
* 000009F-005-01
  * 24697-25084		OK
  * 26568-26950		OK
  * 28429-28811		OK
  * 30324-30708		OK
  * 32285-32669		OK
  * 34174-34561		One base mismatch in forward primer

Smar3
* 000047F
  * 66626-66803		One base mismatch in forward primer

Smar4
* 000001F-013-01	One base mismatch in forward primer
  * 58817-58936
* 000001F		One base mismatch in forward primer
  * 1750659-1750778

Smar5
* 000045F		One base mismatch in reverse primer
  * 291622-292026

Smar6
* 000330F		OK; is this a circular contig? Length would be consistent...
  * 10645-115
* 000336F		First 3 bases of forward primer don't match
  * 8714-9021
* 000224F
  * 5427-5751		OK
  * 13704-14029		OK
  * 22266-22600		OK
  * 31618-31955		OK
* 000195F
  * 4808-5138		OK
  * 14904-15237		OK
  * 25105-25442		OK
  * 34270-34607		OK
  * 44590-44927		OK
* 000009F
  * 1500951-1501288	OK
* 000245F
  * 8573-8902		One base missing in forward primer target
  * 18332-18669		OK
* 000223F
  * 7825-8159
  * 16201-16535
  * 24970-25302
  * 33141-33475
* 000271F
  * 6085-6421
  * 14873-15210
* 000251F
  * 4091-4422
  * 13377-13711
  * 22737-23072
* 000334F
  * 9670-9999
* 000270F
  * 9880-10213
  * 19007-19339
* 000321F
  * 966-1296
  * 9729-10066
* 000257F
  * 6899-7236
  * 16944-17276
...

Smar7
* 000006F
  * Doesn't seem to hit the entirety of the short primer...

Smar8
* 000140F
  * 42745-42932		OK
